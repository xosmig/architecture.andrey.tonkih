module mul10(out, in);
	input [3:0] in;
	output [7:0] out;

	supply0 gnd;

	wire carry[4:6];

	assign out[0] = gnd;
	assign out[1] = in[0];
	assign out[2] = in[1];
	
	sum2 s3(out[3], carry[4], in[0], in[2]);
	sum3 s4(out[4], carry[5], in[1], in[3], carry[4]);
	sum2 s5(out[5], carry[6], in[2], carry[5]);

	// sum2 s67(out[6], out[7], carry[6], in[3]);
	
	assign out[6] = carry[6] | in[3];
	assign out[7] = gnd;
endmodule

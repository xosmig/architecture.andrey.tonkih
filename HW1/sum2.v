module sum2(res0, res1, a, b);
    input a, b;
    output res0, res1;

    assign res0 = a ^ b;
    assign res1 = a & b;
endmodule

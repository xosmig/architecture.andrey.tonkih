module bcd2bin(out, in);
    input [7:0] in;
    output [7:0] out;

    supply0 gnd;

    wire [7:0] tmp1, tmp0;

    mul10 m(tmp1, in[7:4]);
    assign tmp0[3:0] = in[3:0];
    assign tmp0[7:4] = gnd;

    full_adder_8bit fa(out, gnd, tmp1, tmp0, gnd);
endmodule

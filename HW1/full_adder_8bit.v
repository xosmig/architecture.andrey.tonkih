module full_adder_8bit(sum, carry_out, a, b, carry_in);
    output [7:0] sum;
    output carry_out;
    input [7:0] a, b;
    input carry_in;

    wire [6:0] carry;

    sum3 s0(sum[0], carry[0],  a[0], b[0], carry_in);
    sum3 s1(sum[1], carry[1],  a[1], b[1], carry[0]);
    sum3 s2(sum[2], carry[2],  a[2], b[2], carry[1]);
    sum3 s3(sum[3], carry[3],  a[3], b[3], carry[2]);
    sum3 s4(sum[4], carry[4],  a[4], b[4], carry[3]);
    sum3 s5(sum[5], carry[5],  a[5], b[5], carry[4]);
    sum3 s6(sum[6], carry[6],  a[6], b[6], carry[5]);
    sum3 s7(sum[7], carry_out, a[7], b[7], carry[6]);
endmodule

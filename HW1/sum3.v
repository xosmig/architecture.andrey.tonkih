module sum3(res0, res1, a, b, c);
    input a, b, c;
    output res0, res1;

    wire half_sum, carry1, carry2;

    assign res1 = carry1 | carry2;
    sum2 s1(half_sum, carry1, a, b);
    sum2 s2(res0, carry2, half_sum, c);
endmodule

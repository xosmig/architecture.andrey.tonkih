module loop(clk, reset, ready, out, a, b);
    input clk, reset;
    input [7:0] a;
    input [7:0] b;
    output [15:0] out;
    output ready;

    reg [7:0] A;
  	reg [15:0] RESULT, B;
    reg READY, ON_WORK;

    assign ready = READY;
    assign out = RESULT;

    initial begin
    	READY <= 0;
        ON_WORK <= 0;
    end

    always @(posedge clk)
    begin
    	if (reset) 
    	begin
    		A <= a;
    		B <= b;
    		RESULT <= 0;
    		READY <= 0;
            ON_WORK <= 1;
    	end else 
    	begin
            if (ON_WORK)
            begin
        		if (A[0])
        		begin
        			RESULT <= RESULT + B;
    				B <= B << 1;
        			A <= A >> 1;
        		end else
        		begin
        			if (A == 0)
        			begin
        				READY <= 1;
                        ON_WORK <= 0;
        			end else 
        			begin
        				B <= B << 1;
        				A <= A >> 1;
        			end
                end
    		end
    	end
    end
endmodule
